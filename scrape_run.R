library("RSQLite")


source('facebook_scraper.R')

scrape_run <- function(app_id,app_secret,fbpage,days) {
  # register scrape run, get sqlite time?
  today = Sys.time()
  begin = today - days*24*60*60
  data = getPagePostsCommentsLikes(app_id,app_secret,fbpage,
                       format(begin,"%Y-%m-%d"),
                       format(today,"%Y-%m-%d"))
  
  fb_posts = data[[1]]
  fb_comments = data[[2]]
  
  fb_posts$seen = today
  fb_comments$seen = today

  if(nrow(fb_posts)==0) {
    return;
  }
  
  print("doing things")
  

  con <- dbConnect(SQLite(),dbname="ohai.sqlite3")
  dbBegin(con)  
  if(!dbExistsTable(con, 'posts')) {
    dbWriteTable(con,'posts',fb_posts)
    dbWriteTable(con,'comments',fb_comments)
    
    dbSendStatement(con, "ALTER TABLE posts ADD COLUMN first_seen t NULL;")
    dbSendStatement(con, "ALTER TABLE comments ADD COLUMN first_seen t NULL;")
    
    dbSendStatement(con, "CREATE UNIQUE INDEX IF NOT EXISTS posts_u ON posts (id);")
    dbSendStatement(con, "CREATE UNIQUE INDEX IF NOT EXISTS comments_u ON comments (id);")
  } else {
    print("deleting tmp tables")
    
    dbSendStatement(con, 'delete from tmp_posts;')
    dbSendStatement(con, 'delete from tmp_comments;')
    
    print("updating tmp tables")
    dbWriteTable(con, 'tmp_posts', fb_posts, overwrite=T)
    dbWriteTable(con, 'tmp_comments', fb_comments, overwrite=T)
    
    dbSendStatement(con,
        paste0("insert or ignore into posts",
               "(from_id, from_name, message, created_time, type, link, id, likes_count, comments_count, shares_count, page_name, seen) ",
               "select ",
               "from_id, from_name, message, created_time, type, link, id, likes_count, comments_count, shares_count, page_name, seen ",
               "from tmp_posts;"))
    dbSendStatement(con, paste0(
        "insert or ignore into comments ",
        "(from_id, from_name, message, created_time, likes_count, id, seen, page_name, post_id) ",
        "select from_id, from_name, message, created_time, likes_count, id, seen, page_name, post_id ",
        "from tmp_comments;"))
    
    # ensure seen is updated
    dbSendStatement(con, paste0(
      "update posts set ",
      "seen=(select seen from tmp_posts where posts.id=tmp_posts.id), ",
      "page_name=(select page_name from tmp_posts where posts.id=tmp_posts.id) ",
      " where posts.id in (select id from tmp_posts);"
    ))
    dbSendStatement(con, paste0(
      " update comments set ",
      " seen=(select seen from tmp_comments where comments.id=tmp_comments.id),",
      " page_name=(select page_name from tmp_comments where comments.id=tmp_comments.id),",
      " post_id=(select post_id from tmp_comments where comments.id=tmp_comments.id)",
      " where comments.id in (select id from tmp_comments);"))

    
    # TODO: backup old text versions as well
    
    #dbExecute(con, "DROP TABLE tmp_posts");
    #dbExecute(con, "DROP TABLE tmp_comments");
  }
  # update first_seen timestamps
  # TODO: ponder use of db timestamps vs Sys.time
  dbSendStatement(con, "UPDATE posts SET first_seen = CURRENT_TIMESTAMP WHERE first_seen IS NULL;")
  dbSendStatement(con, "UPDATE comments SET first_seen = CURRENT_TIMESTAMP WHERE first_seen IS NULL;")

  dbCommit(con)
  dbDisconnect(con)
}
