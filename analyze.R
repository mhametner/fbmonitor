library("RSQLite")

con <- dbConnect(SQLite(),dbname="ohai.sqlite3")
dbBegin(con)

fb_posts = dbReadTable(con,'posts')
fb_posts$seen = as.POSIXct(fb_posts$seen, origin="1970-01-01")
fb_comments = dbReadTable(con,'comments')
fb_comments$seen = as.POSIXct(fb_comments$seen, origin="1970-01-01")


# legacy data migration
fb_comments[is.na(fb_comments$post_id),]$post_id = as.character(lapply(fb_comments[is.na(fb_comments$post_id),]$id, function(x) {
  y = strsplit(x[[1]],'_',fixed=TRUE)[[1]][[1]]
  z = fb_posts[grepl(y,fb_posts$id),]
  print(nrow(z))
  return(as.character(z$id))
}))
fb_comments[is.na(fb_comments$page_name),]$page_name = as.character(lapply(fb_comments[is.na(fb_comments$page_name),]$post_id, function(x) {
  return(fb_comments[!is.na(fb_comments$page_name) & fb_comments$post_id==x,]$page_name[[1]]);
}))



all_deleted_df = NULL

for(pid in levels(factor(fb_comments$post_id))) {
  tmp_df = fb_comments[fb_comments$post_id==pid,]
  tmp_df = tmp_df[!is.na(tmp_df$id),] # i don't even
  max_seen = max(tmp_df$seen)
  less_df = tmp_df[tmp_df$seen<max_seen,]
  if(is.null(all_deleted_df)) {
    all_deleted_df = less_df
  } else {
    all_deleted_df = rbind(all_deleted_df, less_df)
  }
}

# for comparison
# nrow(fb_comments[fb_comments$created_time>'2016-10-26T22:00',])

dbRollback(con)
dbDisconnect(con)
#### Analyze the data
library(ggplot2)
library(scales)
library(stringr)
library(ggthemes)

fb_comments$created_timet <- substr(fb_comments$created_time, 1, 19) 
fb_comments$created_timet <- strptime(fb_comments$created_timet, "%Y-%m-%dT%H:%M:%S")
fb_comments$created_timeb <- as.Date(fb_comments$created_timet)

strache_deleted = all_deleted_df[all_deleted_df$page_name=='HCStrache',]

strache_deleted$created_timet <- substr(strache_deleted$created_time, 1, 19) 
strache_deleted$created_timet <- strptime(strache_deleted$created_timet, "%Y-%m-%dT%H:%M:%S")
strache_deleted$created_timeb <- as.Date(strache_deleted$created_timet)

cplot <- ggplot(fb_comments, aes(x=created_timeb)) + 
  geom_histogram(binwidth=1, fill = "#7A8FCC", colour="black") +
  scale_x_date(#breaks=date_breaks(width="1 day"),
               #limits = c(as.Date("2016-10-22"), as.Date("2016-10-27"))
    ) +
  ylab("Comments") + 
  theme_fivethirtyeight()+
  xlab("Date")+
  ggtitle("Number of Comments on the \nFacebook Page of HC Strache")
plot(cplot)
ggsave(cplot, file="cplot.png", width=16, height=9, units="cm", device=NULL)

categorizeddata <- read.csv("~/Google Drive/dStd.at/fbmonitor/fbmonitor/fbmonitor/categorizeddata.csv")

categorizeddata$count <- as.numeric(categorizeddata$count)

categorizeddataplot <- ggplot(categorizeddata, aes(x=category, y=reorder(count, -count))) +
  geom_bar(stat='identity') +
  coord_flip() +
  theme_fivethirtyeight() +
  labs(x = "", y = "Count of deleted posts per category") +
  guides(fill=FALSE) +
  ggtitle("Which Posts Are Deleted From HC Straches Page") 
plot(categorizeddataplot)
ggsave(categorizeddata, file="categorizeddata.png", width=16, height=9, units="cm", device=NULL)



dplot <- ggplot(strache_deleted, aes(x=seen)) + 
  geom_histogram(fill = "#7A8FCC", colour="black") +
  #scale_x_date(#breaks=date_breaks(width="1 day"),
    #limits = c(as.Date("2016-10-22"), as.Date("2016-10-27"))
  #) +
  scale_x_datetime(labels = date_format("%H:%M")) +
  ylab("Comments") + 
  theme_fivethirtyeight()+
  xlab("Date")+
  ggtitle("Last seen of deleted Comments on the \nFacebook Page of HC Strache")
plot(dplot)
ggsave(dplot, file="dplot.png", width=16, height=9, units="cm", device=NULL)

dcplot <- ggplot(strache_deleted, aes(x=created_timet)) + 
  geom_histogram(fill = "#7A8FCC", colour="black") +
  #scale_x_date(#breaks=date_breaks(width="1 day"),
  #limits = c(as.Date("2016-10-22"), as.Date("2016-10-27"))
  #) +
  scale_x_datetime(labels = date_format("%H:%M")) +
  ylab("Comments") + 
  theme_fivethirtyeight()+
  xlab("Date")+
  ggtitle("Creation date of deleted Comments on the \nFacebook Page of HC Strache")
plot(dcplot)
ggsave(dcplot, file="dcplot.png", width=16, height=9, units="cm", device=NULL)
