## Facebook Monitor

Facebook admins routinely delete comments on their page, if they don't meet their standards. Or to silence critical voices.

**Usecase**: You want to investigate, which comments on a specific Facebook page are deleted.

**Goal**: Find patterns, which suggest that criticism is censored by admins. 

## What you will need in advance
You will need an App ID from Facebook. 
*myAppID* = app ID from app you created at developer.facebook.com
*myAppSecret* = app secret (see above)

Setup instructions are here:
http://thinktostart.com/analyzing-facebook-with-r/

## Warning

Facebook restricts requests to a maximum of 500 replies (likes, comments).
rfacebook does support paging through all content, but it seems like this is buggy.

This means: a comment might be registered as deleted while actually being online.

You can ping the comment's ID to verify it being offline.
Ascript will be added shortly to help with this.