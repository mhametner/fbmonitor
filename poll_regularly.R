# EXPECTS THE FOLLOWING TO BE SET GLOBALLY:
# - all_id
# - app_secret
# - fb_pages (name or id) (f.e. c("asdf","asdfdfdf") )

source("scrape_run.R")

while(TRUE) {
  print(Sys.time())
  for(fb_page in fb_pages) {
    print(fb_page)
    scrape_run(app_id, app_secret, as.character(fb_page),4)
  }
  
  print("sleeping")
  Sys.sleep(10*60)
}
